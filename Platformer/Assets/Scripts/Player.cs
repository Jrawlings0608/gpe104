﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int playerSpeed = 10;
    private bool facingRight = true;
    public int playerJumpPower = 400;
    private float moveX;
    public bool isGrounded;
    public AudioSource jumpSound;

    // Update is called once per frame
    void Update()
    {
        PlayerMove();
        PlayerRaycast();

    }
    void PlayerMove()
    {
        //Controls
        moveX = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            Jump();
        }
        //Animations
        // Player Direction
        if (moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if (moveX > 0.0f && facingRight == true)
        {
            FlipPlayer();
        }
        //Physics
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }
    void Jump()
    {
        //Jumping Code
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);
        isGrounded = false;
        jumpSound.Play();
    }
    void FlipPlayer()
    {
        //Flips character when going left
        facingRight = !facingRight;
        Vector2 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
            Debug.Log("Player has collided with " + col.collider.name);
        {
            isGrounded = true;
        }
    }
    //Kills enemy and sends downward
    void PlayerRaycast()
    {
        RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.down);
        if (hit.distance < 0.9f && hit.collider.tag == "RockEnemy")
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 100);
            hit.collider.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
            hit.collider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            hit.collider.gameObject.GetComponent<EnemyMove>().enabled = false;
            
            
        if (hit != null && hit.distance <0.9f && hit.collider.tag != "RockEnemy")
            {
                isGrounded = true;
            }
        }
    }
}

