﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PlayerScore : MonoBehaviour
{
    //Sets time and score base.
    private float timeLeft = 400;
    public int playerScore =0;
    public GameObject timeLeftUI;
    public GameObject playerScoreUI;

    // Update is called once per frame
    void Update()
    {
        //Counts down time left.
        timeLeft -= Time.deltaTime;
        timeLeftUI.gameObject.GetComponent<Text>().text = ("Time Left: " + timeLeft);
        playerScoreUI.gameObject.GetComponent<Text>().text = ("Score: " + playerScore);
        if (timeLeft < 0.1f)
        {
            //Reloads the Level
            SceneManager.LoadScene("Platformer1");
        }
    }
    void OnTriggerEnter2D (Collider2D trig)
    {
        if (trig.gameObject.name == "EndLevel")
        {
            CountScore();
        }
            if (trig.gameObject.name == "Coin")
            {
                playerScore += 10;
            Destroy(trig.gameObject);
            }
                

        
        
    }
    void CountScore ()
    {
        playerScore = playerScore + (int)(timeLeft * 10);
        
    }
}
