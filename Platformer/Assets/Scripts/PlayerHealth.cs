﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public int health;
    public bool hasDied;
    // Start is called before the first frame update
    void Start()
    {
        //Not Dead
        hasDied = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Kills player if goes beneath the scene.
        if (gameObject.transform.position.y < -7)
        {
            hasDied = true;
        }
        if (hasDied == true)
        {
            StartCoroutine ("Die");
        }
        
     }
    //Death Routine
    IEnumerator Die ()
        {
        SceneManager.LoadScene("Platformer1");
        yield return null;
    }

}
