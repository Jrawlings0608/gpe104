﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    Transform tf;
    public float speed = 1.0f;
    public float scaleRate = 5.0f;

    // Use this for initialization
    Vector3 startPos;
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        // Move Sprite
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("W key");
            tf.position += transform.up * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            Debug.Log("S key");
            tf.position -= transform.up * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("A key");
            tf.position -= transform.right * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("D key");
            tf.position += transform.right * Time.deltaTime * speed;
       
        }
        if (Input.GetKey(KeyCode.Space))
        {
            transform.position = startPos;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(1.0f, 0.0f, 0.0f);
        }
    }
}
