﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableComponents : MonoBehaviour
{
    private PlayerControl EnableDisable;

    void Start()
    {
        EnableDisable=GetComponent<PlayerControl>();
    }

  
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.P))
        {
            EnableDisable.enabled = !EnableDisable.enabled;
        }
    }
}
