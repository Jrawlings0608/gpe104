﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seeing : MonoBehaviour
{
    public float FieldOfView = 45f;
    public bool seeing = false;
    public bool IsSeeingPlayer()
    {
        return seeing;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            if (Vector3.Angle(collision.transform.position - transform.position,transform.right) <= FieldOfView)
            {
                var hit = Physics2D.Raycast(transform.position, (collision.transform.position - transform.position).normalized, 100);

                if (hit.collider.gameObject.name == "Player")
                {
                    seeing = true;
                    return;
                }
            }
        }
        seeing = false;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            seeing = false;
        }
    }
}