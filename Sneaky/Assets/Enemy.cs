﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Hearing hearingController;
    private Seeing SightController;
    // Start is called before the first frame update
    void Start()
    {
        hearingController = GetComponentInChildren<Hearing>();
        SightController = GetComponentInChildren<Seeing>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
